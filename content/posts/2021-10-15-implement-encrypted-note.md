---
title: "Implement Password Protected Note in note.warungkode.net"
date: 2021-10-15T16:34:23+07:00
draft: false
---

Today i need to continue my work on implementing password protected note on my own [notepad.pw](https://notepad.pw) like snippet site: [note.warungkode.net](https://note.warungkode.net).

Last time i found an clasic programmers problem: "It works on my local, but error in deployment" like this: [issue](https://github.com/aquaswim/notepad-firebase/pull/25)

![Error screenshoot](https://user-images.githubusercontent.com/31547551/132094412-83762446-5608-4cb7-a1fd-b6bf59724326.png)
---
title: "Test Upload Image (My Keyboard)"
date: 2021-11-28T22:23:23+07:00
draft: false
---

Testing image with jekyl (just an excuse to upload my keyboard lol)

Here is my keyboard, this is **KeyboardParadise VX80 TKL** with Cherry MX Brown Switch
![Before](</2021-11-28-test-image/photo_2021-11-28 22.18.02.jpeg>)

And recently i bought jp layout white keycaps (IDK what the name, i buy second hand)
![I Buy Keycaps](</2021-11-28-test-image/photo_2021-11-28 22.18.08.jpeg>)

Here is the image after i install the keycaps to my keyboard.
![After installing keyboard](</2021-11-28-test-image/photo_2021-11-28 22.18.09.jpeg>)

The color is inspired by Oreo
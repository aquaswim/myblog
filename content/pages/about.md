---
title: 'About Me'
button: 'about'
weight: 2
---

Hi! 👋, my name is bayu "aquaswim" setiawan, welcome to my personal blog.
I wrote anything that pick my interest and something that i work on (or tried to do).

I mostly spent my spare time learning new stuff in programming, and playing game 🙈.

### Don't hesitate to say hello when we meet @

* [github](https://github.com/aquaswim)
* [twitter](https://twitter.com/Little_bayu)
* [osu! game](https://osu.ppy.sh/u/aquaswim)